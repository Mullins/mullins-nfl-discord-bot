var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
var nflscores = require('nfl_scores');
var fs = require('fs');
var schedule = require('node-schedule');
var async = require('async');
var oneLinerJoke = require('one-liner-joke');
var backronym = require('backronym');
var motivation = require('motivation');
var movieQuotes = require('movie-quotes');
var predator = require('predator-quotes');


// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});

logger.level = 'debug';

var gameScores = new Array();

nflscores.refresh(function(err, scores) {
    if (scores.games) {    
        fs.writeFile('./games.json', JSON.stringify(scores.games), function(err) {
            if (err) {
                logger.info("ERROR WRITING FILE:");
                logger.info(err);
            }
        });

        gamesScores = parseGames(scores.games);
    }
});

var sched = schedule.scheduleJob('*/5 * * 1,9,10,11,12 0,4,6', function() {
    var nfl;

    fs.readFile('./games.json', function(err, data) {
        if (!err) {
            if (data.games) {
                gamesScores = parseGames(JSON.parse(data.games));
            }
        }
        else {
            logger.info("Error reading file:");
            logger.info(err);
        }
    });

    nflscores.refresh(function(err, scores) {
        if (scores.games) {
            nfl = scores.games || "";
    
            if (nfl != "") {
                fs.writeFile('./games.json', JSON.stringify(nfl), function(err) {
                    if (err) {
                        logger.info("ERROR:");
                        logger.info(err);
                    }
                });
            }
        }
    });
});

function parseGames(games) {
    var myGames = {};
    for (var i = 0; i < games.length; i++) {
        myGames[games[i]['h']] = {};

        myGames[games[i]['v']] = {};

        myGames[games[i]['h']]['nickName'] = games[i]['hnn'];
        myGames[games[i]['h']]['opponentShortName'] = games[i]['v'];
        myGames[games[i]['h']]['opponentNickName'] = games[i]['vnn'];
        myGames[games[i]['h']]['home'] = 1;
        myGames[games[i]['h']]['when'] = games[i]['d'] + ' - ' + games[i]['t'];
        myGames[games[i]['h']]['myScore'] = games[i]['hs'] || "";
        myGames[games[i]['h']]['theirScore'] = games[i]['vs'] || "";

        myGames[games[i]['v']]['nickName'] = games[i]['vnn'];
        myGames[games[i]['v']]['opponentShortName'] = games[i]['h'];
        myGames[games[i]['v']]['opponentNickName'] = games[i]['hnn'];
        myGames[games[i]['v']]['home'] = 0;
        myGames[games[i]['v']]['when'] = games[i]['d'] + ' at ' + games[i]['t'];
        myGames[games[i]['v']]['myScore'] = games[i]['vs'] || "";
        myGames[games[i]['v']]['theirScore'] = games[i]['hs'] || "";
    };

    return myGames;
}


// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        var extra = "";

        if (args.length > 1) {
            extra = args[1] || "";
        }
       
        args = args.splice(1);
        switch(cmd) {
            case 'list':
            case 'help':
                var msg = 'All commands must start with a "!"\n';
                msg += 'Type "!ping" to verify the bot is working\n';
                msg += 'Type "!hello" for a welcome message\n';
                msg += 'Type "!" plus your favorite NFL team for info about their next game\n';
                msg += 'Type "!joke" for a random one-liner\n';
                msg += 'Type "!what" plus a string of letters (ex. cia) to get an acronym\n';
                msg += 'Type "!motivate" for a motivational message\n';
                msg += 'Type "!movie" for a random movie quote\n';
                msg += 'Type "!predator" for a random quote from Predator (warning: may contain offensive language)\n';
                bot.sendMessage({
                    to: channelID,
                    message: msg
                });
            break;
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            break;
            case 'hello':
                bot.sendMessage({
                    to: channelID,
                    message: 'Welcome!'
                });
            break;
            case 'joke':
                var myJoke = oneLinerJoke.getRandomJoke();
                //myJoke = JSON.parse(myJoke);
                bot.sendMessage({
                    to: channelID,
                    message: myJoke.body
                });
            break;
            case 'what':
                var whatMsg;
                if (extra != "") {
                    whatMsg = backronym.create(extra);
                }
                else {
                    whatMsg = "Please pass a sting of letters";
                }

                bot.sendMessage({
                    to: channelID,
                    message: whatMsg
                });
            break;
            case 'motivate':
                var m = motivation.get();
                var mMsg = m.text + " - " + m.author;

                bot.sendMessage({
                    to: channelID,
                    message: mMsg
                });
            break;
            case 'movie':
                var mq = movieQuotes.random();

                bot.sendMessage({
                    to: channelID,
                    message: mq
                });
            break;
            case 'predator':
                pq = predator.random();

                bot.sendMessage({
                    to: channelID,
                    message: pq
                });
            break;
            case 'nfl':
                bot.sendMessage({
                    to: channelID,
                    message: 'Please type "!" plus your team...'
                });
            break;
            case 'det':
            case 'detroit':
            case 'Detroit':
            case 'DET':
            case 'Lions':
            case 'LIONS':
            case 'lions':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'DET';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'pit':
            case 'pittsburgh':
            case 'Pittsburgh':
            case 'PIT':
            case 'Steelers':
            case 'STEELERS':
            case 'steelers':
            case 'pitt':
            case 'PITT':
            case 'Pitt':
                fs.readFile('./games.json', function(err, data) {
                    var teame = 'PIT';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'ten':
            case 'tennessee':
            case 'Tennessee':
            case 'TEN':
            case 'Titans':
            case 'TITANS':
            case 'titans':
            case 'tenn':
            case 'Tenn':
            case 'TENN':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'TEN';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'chi':
            case 'chicago':
            case 'Chicago':
            case 'CHI':
            case 'Bears':
            case 'BEARS':
            case 'bears':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'CHI';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'CLE':
            case 'cleveland':
            case 'Cleveland':
            case 'cle':
            case 'Browns':
            case 'BROWNS':
            case 'browns':
            case 'losers':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'CLE';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'jax':
            case 'jacksonville':
            case 'Jacksonville':
            case 'JAX':
            case 'Jaguars':
            case 'JAGUARS':
            case 'jaguars':
            case 'jack':
            case 'JACK':
            case 'Jack':
            case 'jacks':
            case 'Jacks':
            case 'JACKS':
            case 'jags':
            case 'Jags':
            case 'JAGS':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'JAX';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'gb':
            case 'Green Bay':
            case 'green bay':
            case 'GB':
            case 'Packers':
            case 'Packers':
            case 'packers':
            case 'pack':
            case 'Pack':
            case 'PACK':
            case 'GreenBay':
            case 'greenbay':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'GB';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'bal':
            case 'Baltimore':
            case 'baltimore':
            case 'BAL':
            case 'Ravens':
            case 'RAVENS':
            case 'ravens':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'BAL';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'hou':
            case 'houston':
            case 'Houston':
            case 'HOU':
            case 'Texans':
            case 'TEXANS':
            case 'texans':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'HOU';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'ari':
            case 'arizona':
            case 'Arizona':
            case 'ARI':
            case 'Cardinals':
            case 'CARDINALS':
            case 'cardinals':
            case 'cards':
            case 'CARDS':
            case 'Cards':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'ARI';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'MIA':
            case 'miami':
            case 'Miami':
            case 'MIA':
            case 'dolphins':
            case 'DOLPHINS':
            case 'Dolphins':
            case 'fins':
            case 'Fins':
            case 'FINS':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'MIA';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'tb':
            case 'tampa bay':
            case 'Tampa Bay':
            case 'TB':
            case 'Buccaneers':
            case 'BUCCANEERS':
            case 'buccaneers':
            case 'tampabay':
            case 'TampaBay':
            case 'tampa':
            case 'TAMPA':
            case 'Tampa':
            case 'Bucs':
            case 'BUCS':
            case 'bucs':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'TB';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'min':
            case 'minnesota':
            case 'Minnesota':
            case 'MIN':
            case 'Min':
            case 'Vikings':
            case 'VIKINGS':
            case 'vikings':
            case 'vikes':
            case 'Vikes':
            case 'VIKES':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'MIN';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'lar':
            case 'LAR':
            case 'Lar':
            case 'Rams':
            case 'RAMS':
            case 'rams':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'LA';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'no':
            case 'new orleans':
            case 'New Orleans':
            case 'NO':
            case 'N.O.':
            case 'n.o.':
            case 'Saints':
            case 'SAINTS':
            case 'saints':
            case 'NewOrleans':
            case 'neworleans':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'NO';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'was':
            case 'washington':
            case 'Washington':
            case 'WAS':
            case 'Redskins':
            case 'REDSKINS':
            case 'redskins':
            case 'skins':
            case 'Skins':
            case 'wash':
            case 'Wash':
            case 'WASH':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'WAS';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'nyg':
            case 'NYG':
            case 'Giants':
            case 'GIANTS':
            case 'giants':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'NYG';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            mdg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'kc':
            case 'Kansas City':
            case 'kansas city':
            case 'KC':
            case 'Chiefs':
            case 'CHIEFS':
            case 'chiefs':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'KC';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'LAC':
            case 'chargers':
            case 'San Diego':
            case 'san diego':
            case 'Chargers':
            case 'CHARGERS':
            case 'lac':
            case 'sandiego':
            case 'SanDiego':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'LAC';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'buf':
            case 'buffalo':
            case 'Buffalo':
            case 'BUF':
            case 'Bills':
            case 'BILLS':
            case 'bills':
            case 'buff':
            case 'Buff':
            case 'BUFF':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'BUF';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'den':
            case 'denver':
            case 'Denver':
            case 'DEN':
            case 'Broncos':
            case 'BRONCOS':
            case 'broncos':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'DEN';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'cin':
            case 'cincinnati':
            case 'Cincinnati':
            case 'CIN':
            case 'Bengals':
            case 'BENGALS':
            case 'bengals':
            case 'cincy':
            case 'CINCY':
            case 'Cincy':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'CIN';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]['myScore'] != "") {
                            msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                        }
                        else if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];
                            
                            if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'oak':
            case 'oakland':
            case 'Oakland':
            case 'OAK':
            case 'Raiders':
            case 'RAIDERS':
            case 'raiders':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'OAK';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]['myScore'] != "") {
                            msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                        }
                        else if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }
                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'ne':
            case 'new england':
            case 'New England':
            case 'NE':
            case 'Patriots':
            case 'PATRIOTS':
            case 'patriots':
            case 'newengland':
            case 'NewEngland':
            case 'pats':
            case 'PATS':
            case 'Pats':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'NE';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'dal':
            case 'dallas':
            case 'Dallas':
            case 'DAL':
            case 'Cowboys':
            case 'COWBOYS':
            case 'cowboys':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'DAL';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'PHI':
            case 'philadelphia':
            case 'Philadelphia':
            case 'phi':
            case 'Eagles':
            case 'EAGLES':
            case 'eagles':
            case 'PHILLY':
            case 'philly':
            case 'Philly':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'PHI';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'SEA':
            case 'seattle':
            case 'Seattle':
            case 'sea':
            case 'Seahawks':
            case 'SEAHAWKS':
            case 'seahawks':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'SEA';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'atl':
            case 'atlanta':
            case 'Atlanta':
            case 'ATL':
            case 'Falcons':
            case 'FALCONS':
            case 'falcons':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'ATL';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'nyj':
            case 'NYJ':
            case 'Jets':
            case 'JETS':
            case 'jets':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'NYJ';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'san':
            case 'SAN':
            case 'San Francisco':
            case 'san francisco':
            case 'sanfrancisco':
            case 'SanFrancisco':
            case 'san fran':
            case 'sanfran':
            case 'San Fran':
            case 'SanFran':
            case 'cisco':
            case 'Cisco':
            case '49ers':
            case '49ERS':
            case 'niners':
            case 'NINERS':
            case 'Niners':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'SAN';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));

                        var msg = '';

                        if (gameScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }
                        
                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'car':
            case 'CAR':
            case 'Car':
            case 'Carolina':
            case 'carolina':
            case 'Panthers':
            case 'PANTHERS':
            case 'panthers':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'CAR';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';
                        
                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];

                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'IND':
            case 'ind':
            case 'INDY':
            case 'indy':
            case 'Indy':
            case 'Ind':
            case 'Colts':
            case 'COLTS':
            case 'colts':
                fs.readFile('./games.json', function(err, data) {
                    var team = 'IND';
                    
                    if (!err) {
                        gamesScores = parseGames(JSON.parse(data));
                        
                        var msg = '';

                        if (gamesScores[team]) {
                            var opponent = gamesScores[team]['opponentShortName'];
                            var when = gamesScores[team]['when'];
 
                            if (gamesScores[team]['myScore'] != "") {
                                msg = team + ": " + gamesScores[team]['myScore'] + " " + opponent + ": " + gamesScores[team]['theirScore'];
                            }
                            else if (gamesScores[team]['home'] === 1) {
                                msg = team + ' is playing ' + opponent + ' on ' + when;
                            }
                            else {
                                msg = team + ' is playing at ' + opponent + ' on ' + when;
                            }
                        }
                        else {
                            msg = team + ' does not have a game this week';
                        }

                        bot.sendMessage({
                            to: channelID,
                            message: msg
                        });
                    }
                    else {
                        logger.info("Error reading file:");
                        logger.info(err);
                    }
                });
            break;
            case 'NY':
            case 'ny':
            case 'Ny':
            case 'New York':
            case 'new york':
            case 'NEW YORK':
            case 'NewYork':
            case 'newyork':
            case 'NEWYORK':
            case 'NYC':
            case 'nyc':
            case 'Nyc':
                bot.sendMessage({
                    to: channelID,
                    message: "Must specify the Jets or the Giants"
                });
            break;
            case 'LA':
            case 'la':
            case 'L.A.':
            case 'Los Angeles':
            case 'los angeles':
            case 'LOS ANGELES':
            case 'LosAngeles':
            case 'losangeles':
            case 'LOSANGELES':
            case 'L.a.':
                bot.sendMessage({
                    to: channelID,
                    message: "Must specify the Rams or the Chargers"
                });
            break;         
            default:
                bot.sendMessage({
                    to: channelID,
                    message: "I am sorry, but I did not understand your command."
                });
            break;

            // Just add any case commands if you want to..
         }
     }
});
