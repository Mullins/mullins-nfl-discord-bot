# Mullins' Discord Bot

A very simple Discord bot written in Node.  It uses the *nfl_scores* Node module to get the current week's NFL games and scores when available.  Type and exclamation point (!) plus the name of your favorite NFL team (ex. 'det', 'detroit', or 'lions') and it will show their current score or next opponent.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

You will need to register a Bot on Discord and get the token.
Place the token in auth.json
```
{
    "token": "XXXXXXXXXXXXXXXXXXXX.XXXXXXXXXXX.XXXXX_XXXXXXXXX.XX_XX"
}
```

Node.js
```
sudo pacman -S node
```
npm
```
sudo pacman -S npm
```

### Installing
Clone the repo

```
git clone git@gitlab.com:Mullins/mullins-nfl-discord-bot.git
```
Enter the project directory
```
cd mullinsBot
```

Install Node modules
```
npm install
```

Should see something similiar to this:
```
info: Connected
info: Logged in as: 
info: mullins-bot - (xxxxxxxxxxxxxxxxxx)
```

## Built With

* [Node.js](https://nodejs.org)
* [nfl_scores](https://www.npmjs.com/package/nfl_scores) - Used to get NFL games and scores
* [node-schedule](https://www.npmjs.com/package/node-schedule) - Used to schedule automatic retrieval of NFL scores and games using *nfl_scores* module

## Authors

* **Nicholas Mullins** - [Mullins](https://gitlab.com/Mullins/)

## License

This project is licensed under the MIT License

## TODO

* Clean up the code
* Move the reading of the JSON file into a single method/function
* Make the output "prettier"
* Add additional responses
