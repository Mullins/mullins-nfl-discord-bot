
//getGames();

module.exports = { 
    getGames: function () {
        console.log("...getting games");
        var games = new Array();
        loadJSON('js/games.json', function(data) {
                games = parseGames(data.games);}, 
                function(xhr) { console.log(xhr); }
        );

        //console.log("GAMES: " + JSON.stringify(games));
        return games;
    }
};

function loadJSON(path, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success) {
                    success(JSON.parse(xhr.responseText));
                }
                else {
                    if (error) {
                        error(xhr);
                    }
                }
            }
        }
    };

    xhr.open("GET", path, true);
    xhr.send();
}

function parseGames(games) {
    var myGames = new Array();
    console.log("GAME: " + JSON.stringify(games));
    //$.each(games, function(game) {
    for (var i = 0; i < games.length; i++) {
        var game = i;

        myGames.push(games[game]['h']);

        myGames[game] = {};
        myGames[game]['nickName'] = games[game]['hnn'];
        myGames[game]['opponentShortName'] = games[game]['v'];
        myGames[game]['opponentNickName'] = games[game]['vnn'];
        
        //console.log(JSON.stringify(myGames));
    };

    //console.log("MYGAMES: " + JSON.stringify(myGames));
    return myGames;
}